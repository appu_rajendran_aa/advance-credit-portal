import React, { useEffect, useState } from "react";
import DefaultLayout from "@/layout/default";
import Header from "@/components/common/Header";
import AuthNavbar from "@/components/common/Navbar"
import BreadCrumb from "@/components/common/BreadCrumb"
import AccountSummary from "@/components/common/AccountSummary";
import RecentTransaction from "@/components/common/RecentTransaction";
import OpenToBuy from "@/components/common/OpenToBuy";
import {
  Row,
  Col,
  Container
} from "reactstrap";
const Dashboard = (props) => {
  let [availablePercentage, setAvailablePercentage] = React.useState(0);
  return (
    <>
      <Header />
      <AuthNavbar />
      <Container className="mt--12" fluid>
        <BreadCrumb />
        <Row className="mt-5">
          <Col xl="5">
            <AccountSummary setAvailablePercentage={setAvailablePercentage} />
          </Col>
          <Col className="mb-5 mb-xl-0" xl="7">
            <RecentTransaction />
          </Col>
        </Row>
        <Row className="header pb-2 pt-2 pt-md-4">
          <Col className="mb-5 mb-xl-0" xl="5">
            <OpenToBuy availablePercentage={availablePercentage} />
          </Col>
          <Col xl="8">
          </Col>
        </Row>
      </Container>
    </>
  );
};
//Get page specific Layout.
Dashboard.getLayout = function getLayout(page) {
  return (
    <DefaultLayout>
      {page}
    </DefaultLayout>
  )
}

export default Dashboard;