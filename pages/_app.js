import "@/styles/css/aap-necleo.css";
import "@fortawesome/fontawesome-free";
import "@/styles/scss/aap-billing-portal-dashboard.scss";
import Router from "next/router";
import ReactDOM from "react-dom";
import PreLoader from "@/components/common/PreLoader";

Router.events.on("routeChangeStart", (url) => {
  console.log(`Loading: ${url}`);
  document.body.classList.add("body-page-transition");
  ReactDOM.render(
    <PreLoader path={url} />,
    document.getElementById("page-transition")
  );
});
Router.events.on("routeChangeComplete", () => {
  ReactDOM.unmountComponentAtNode(document.getElementById("page-transition"));
  document.body.classList.remove("body-page-transition");
});
Router.events.on("routeChangeError", () => {
  ReactDOM.unmountComponentAtNode(document.getElementById("page-transition"));
  document.body.classList.remove("body-page-transition");
});



const AapMainApp = ({ Component, pageProps }) => {
  const getLayout = Component.getLayout || ((page) => page)
  return getLayout(<Component {...pageProps} />);
}

export default AapMainApp
