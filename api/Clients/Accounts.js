// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

// export default function handler(req, res) {
//   res.status(200).json({ name: 'John Doe' })
// }


import { ApiClient } from '@/api/Middleware/Dashboard'
let client = new ApiClient(process.env.NEXT_PUBLIC_API)

export default {
  accountSummary(data) {
    return client.get('customers/'+data+'/account-summary', data)
  },
}