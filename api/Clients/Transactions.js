// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

// export default function handler(req, res) {
//   res.status(200).json({ name: 'John Doe' })
// }


import { ApiClient } from '@/api/Middleware/Dashboard'
let client = new ApiClient(process.env.NEXT_PUBLIC_API2)

export default {
  recentTransactions(data) {

    return client.get('transactions/'+data+'/recent-transactions', data)
  },
}