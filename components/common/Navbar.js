import React, { useEffect } from "react";
import i18n from "./../Translations/i18n"
import { useTranslation } from "react-i18next";
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  Button
} from "reactstrap";
import Router, { useRouter } from 'next/router';
import { route } from "next/dist/server/router";
import NavItems from "./NavItems";
import Collapsible from 'react-collapsible';
const AdminNavbar = () => {
  let [selectedLanguage, setselectedLanguage] = React.useState("English");
  const router = useRouter();
  const languageList = process.env.NEXT_PUBLIC_LANGUAGE;


  const toggleOption = (e) => {
    console.log(e)
    e.preventDefault();
    if (e.target.name == "language") {
      setselectedLanguage(e.target.innerText);
      Router.push(`/dashboard/${e.target.id}`)
      setTimeout(() => {
        Router.reload()
      }, 300)
    }
  }
  const { t } = useTranslation();

  const handleOpen = () => {
    document.getElementById("myNav").style.height = "100%";
  }
  const handleClose = () => {
    document.getElementById("myNav").style.height = "0%";
  }
  return (
    <>

      <Navbar className="navbar navbar-expand-lg navbar-horizontal navbar-dark p-0" style={{ position: "sticky", top: " 0px", zIndex: "555", background: "#cc0033" }} expand="md">
        <Button onClick={() => handleOpen()} className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="p-3 navbar-toggler-icon"></span>
        </Button>
        <Container className="px-0 nav-container">
          <div id="myNav" className="overlay">
            <a className="closebtn" onClick={() => handleClose()}>&times;</a>
            <div className="overlay-content">
              <a>{t('home')}</a>
              <a>
                <Collapsible trigger={t('payment')}>
                  <li className="nav-item w-95 row ml-3 mt-2">
                    <label className="d-block m-0 w-100">
                      <span className="small inline-block w-53"> {t("make_A_Payment")}</span>
                    </label>
                  </li>
                  <li className="nav-item w-95 row ml-3">
                    <label className="d-block w-100 m-0">
                      <span className="small inline-block w-53" >{t("Pending_Payments")}</span>
                    </label>
                  </li>
                  <li className="nav-item w-95 row ml-3">
                    <label className="d-block w-100 m-0">
                      <span className="small inline-block w-53">{t("Payment_History")}</span>
                    </label>
                  </li>
                  <li className="nav-item w-90 row ml-3">
                    <label className="d-block w-100 m-0">
                      <span className="small inline-block w-53"> {t("Automatic_Payments")}</span>
                    </label>
                  </li>
                  <li className="nav-item w-95 row ml-3">
                    <label className="d-block w-100 m-0">
                      <span className="small inline-block w-53">{t("Saved_Payment_Cart")}</span>
                    </label>
                  </li>
                  <li className="nav-item w-90 row ml-3">
                    <label className="d-block w-100 m-0">
                      <span className="small inline-block w-53"> {t("Payment_Sources")}</span>
                    </label>
                  </li>
                </Collapsible>
              </a>
              <a>{t('statements')}</a>
              <a>{t('manage_Account')}</a>
            </div>
          </div>
          <NavItems />
          <div className="right-wing-menu">
            <UncontrolledDropdown className="right-wing-menu">
              <DropdownToggle className="more-options nav-drop-down"
              >
                {t("i_Want_To")}
              </DropdownToggle>
              <DropdownMenu end="true">
                <DropdownItem href="#">
                  {t("Dispute_an_Invoice")}
                </DropdownItem>
                <DropdownItem href="#">
                  {t("Manage_Payment_Sources")}
                </DropdownItem>
                <DropdownItem href="#">
                  {t("Update_My_Profile")}
                </DropdownItem>
                <DropdownItem href="#">
                  {t("See_More_Options")}
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <UncontrolledDropdown
            >
              <DropdownToggle className="langauge1 nav-drop-down"
              >
                {selectedLanguage}
              </DropdownToggle>
              <DropdownMenu end="true">
                {languageList && JSON.parse(languageList).map((c, i) => (
                  <DropdownItem className="langauge" name="language" key={i} id={c.code} onClick={(e) => toggleOption(e)}>
                    {c.language}
                  </DropdownItem>
                ))
                }

              </DropdownMenu>
            </UncontrolledDropdown>
          </div>
        </Container>
      </Navbar>
    </>
  );
}

export default AdminNavbar;