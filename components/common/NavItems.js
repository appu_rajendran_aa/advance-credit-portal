import React, { useEffect } from "react";
import Link from "next/link";
import {
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Navbar,
    NavItem,
    NavLink,
    Nav,
    Container,
    Button
} from "reactstrap";
import { useTranslation } from "react-i18next";
const NavItems = () => {
    const { t } = useTranslation();

    return (
        <Nav className="mr-auto collapse navbar-collapse" navbar>
            <NavItem>
                <Link href="/dash board" passHref>
                    <NavLink href="#pablo" className="nav-link-icon">
                        <span className="home nav-link-inner--text">{t('home')}</span>
                    </NavLink>
                </Link>
            </NavItem>
            <UncontrolledDropdown
                inNavbar
                nav
            >
                <DropdownToggle
                    caret
                    nav
                    className="payment"
                >
                    {t('payment')}
                </DropdownToggle>
                <DropdownMenu end="true">
                    <DropdownItem href="/payments/invoices">
                        {t("make_A_Payment")}
                    </DropdownItem>
                    <DropdownItem href="/payments/payments">
                        {t("Pending_Payments")}
                    </DropdownItem>
                    <DropdownItem href="/payments/invoices">
                        {t("Payment_History")}
                    </DropdownItem>
                    <DropdownItem href="/payments/invoices">
                        {t("Automatic_Payments")}
                    </DropdownItem>
                    <DropdownItem href="/payments/invoices">
                        {t("Saved_Payment_Cart")}
                    </DropdownItem>
                    {/* <DropdownItem divider /> */}
                    <DropdownItem href="/payments/invoices">
                        {t("Payment_Sources")}
                    </DropdownItem>
                </DropdownMenu>
            </UncontrolledDropdown>
            <NavItem>
                {/* <Link href="/auth/login"> */}
                <Link href="#" passHref>
                    <NavLink href="#pablo" className="nav-link-icon">
                        <span className="statement nav-link-inner--text">{t('statements')}</span>
                    </NavLink>
                </Link>
            </NavItem>
            <NavItem>
                {/* <Link href="/admin/profile"> */}
                <Link href="/payments/accounts" passHref>
                    <NavLink className=" nav-link-icon" href="/payments/accounts">
                        <span className="manage-account nav-link-inner--text">{t('manage_Account')}</span>
                    </NavLink>
                </Link>
            </NavItem>
        </Nav>
    )
}
export default NavItems;