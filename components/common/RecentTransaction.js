import React, { useEffect } from "react";
import {
    Card,
    CardHeader,
    Row,
    Col
} from "reactstrap"; 3
import Grid from "../grid/grid";
import Transactions from "@/api/Clients/Transactions";
import i18n from "./../Translations/i18n"
import { useTranslation } from "react-i18next";
import Router, { useRouter } from 'next/router';
import InvoiceModal from "../modal/invoiceModal";
const RecentTransaction = () => {
    const router = useRouter();
    const [modalOpen, setModalOpen] = React.useState(false);
    const [modalData, setModalData] = React.useState(null)
    useEffect(() => {
        recentTransactions()
    }, []);

    let [recentTransaction, setRecentTransaction] = React.useState([]);
    const recentTransactions = async () => {
        try {
            let accountNumber = 21001;
            if (accountNumber) {
                const response = await Transactions.recentTransactions(accountNumber)

                if (response.status == 200) {
                    setRecentTransaction(response.data.content)
                }
                else {
                    setRecentTransaction(null)

                }

            }


        }
        catch (e) {
            console.log(e)
        }

    }
    const switchModal = (data) => {
        setModalOpen(true)
        recentTransaction.forEach((element) => {
            if(element.purchaseOrder == data){
                setModalData(element)
            }
        });
    }
    const { t } = useTranslation();
    return (
        <Card className="shadow margin-top-row-1">
            <CardHeader className="border-0">
                <Row className="align-items-center">
                    <div className="col">
                        <h3 className="mb-0">{t("recent_Transaction")}</h3>
                    </div>

                </Row>
            </CardHeader>
            <Row className="align-items-center">
                <Col className="mb-5 mb-xl-0" xl="12">
                    <Grid data={recentTransaction} switchModal={switchModal}/>
                </Col>
            </Row>
            {modalOpen &&
                <InvoiceModal modalData={modalData} toggle={() => setModalOpen(!modalOpen)} modalOpen={modalOpen}></InvoiceModal>
            }
        </Card>
    )
}
export default RecentTransaction;