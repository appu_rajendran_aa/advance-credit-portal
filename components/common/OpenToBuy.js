import React, { useEffect, useState } from "react";
import {
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    Button,
    Card,
    CardHeader,
    CardBody,
    Row,
    FormGroup,
    Form,
    InputGroup,
    Input
} from "reactstrap";
import Pie from "../charts/pie";
import i18n from "./../Translations/i18n"
import { useTranslation } from "react-i18next";
import Router, { useRouter } from 'next/router';
const OpenToBuy = ({ availablePercentage }) => {
    const router = useRouter();
    const { t } = useTranslation();
    return (
        <Card className="shadow">
            <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                    <div className="col">
                        <div className="col">
                            <h3 className="mb-0">{t("open_To_Buy")}</h3>
                        </div>
                    </div>
                </Row>
            </CardHeader>
            <CardBody>
                <Pie availablePercentage={Number(availablePercentage)}></Pie>
            </CardBody>
        </Card>
    )
}
export default OpenToBuy