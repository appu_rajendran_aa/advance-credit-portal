import React, { useEffect } from "react";

import {
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    Button,
    Card,
    CardHeader,
    Row,
    Col,
    FormGroup,
    Form,
    InputGroup,
} from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Accounts from "@/api/Clients/Accounts";
import i18n from "./../Translations/i18n"
import { useTranslation } from "react-i18next";
import Router, { useRouter } from 'next/router';
const AccountSummary = ({ setAvailablePercentage }) => {
    const router = useRouter();
    const languageList = process.env.NEXT_PUBLIC_LANGUAGE;
    useEffect(() => {
        accountSummaryDetails()

    }, []);


    let [selectedCard, setselectedCard] = React.useState("PAYMENT SOURCE");
    let [selectedAmount, setselectedAmount] = React.useState("PAYMENT AMOUNT");
    let [paymentDate, setpaymentDate] = React.useState('23-DEC-2021');
    let [accountData, setAccountData] = React.useState(null);
    const toggleOption = (e) => {
        e.preventDefault();
        if (e.target.name == "amount") {
            setselectedAmount(e.target.innerText);
        }
        if (e.target.name == "card") {
            setselectedCard(e.target.innerText)
        }

    };
    const toggleOptionDate = (e) => {


        let today = new Date();


        setpaymentDate(e.toShortFormat())

    };

    Date.prototype.toShortFormat = function () {

        let monthNames = ["Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec"];

        let day = this.getDate();

        let monthIndex = this.getMonth();
        let monthName = monthNames[monthIndex];

        let year = this.getFullYear();

        return `${day}-${monthName}-${year}`;
    }
    const accountSummaryDetails = async () => {
        try {
            let accountNumber = 2104;
            const response = await Accounts.accountSummary(accountNumber)
            console.log(response)
            if (response.status == 200) {
                setAccountData(response.data)
                setAvailablePercentage(response.data.totalAvailableCreditPercentage)
            }
        }
        catch (e) {
            console.log(e)
        }

    }
   
    const { t } = useTranslation();
    return (
        <Card className="shadow">
            <CardHeader className="border-0">
                <Row className="align-items-center">
                    <div className="col">
                        <h3 className="mb-0">{t("account_Summary")}</h3>
                    </div>
                    <div className="col text-right">

                    </div>
                </Row>
            </CardHeader>
            <Row className="align-items-center">
                <Col xl="5">
                    <h5 className="ml-4 text-uppercase">{t("account_Summary")}</h5>
                    <h1 className="ml-4">{accountData ? accountData.totalAvailableCredit : "0000.00"}</h1>
                    <h5 className="ml-4">{t("statement_Balance")}: {accountData ? accountData.totalAmountDueRemaining : "0000"}</h5>
                    <h5 className="ml-4" style={{ textDecoration: "underline" }}>{accountData ? accountData.lastStatementDueDate : "XX-XXX-XX"} Statement</h5>
                    <h5 className="ml-4 text-uppercase"> {t("balance_Due")}</h5>
                    <h1 className="ml-4">{accountData ? accountData.lastStatementDueAmount : "0000"}</h1>
                    <h5 className="ml-4"> {t("payment_Due")}</h5>
                    <h5 className="ml-4 text-uppercase"> {t("last_Payment")}</h5>
                    <h1 className="ml-4">{accountData ? accountData.lastPaymentAmount : "0000.00"}</h1>
                    <h5 className="ml-4"> {t("payment_Date")}: {accountData ? accountData.lastPaymentDate : "XX-XXX-XX"}</h5>

                    <Row className="col mt-4 ml-2 row">

                        {/* <i className="ni ni-credit-card pr-2" /> */}
                        <h6 className="" > {t("no_Scheduled_Auto_Payment")}</h6>
                    </Row>
                </Col>
                <Col xl="6" className="mx-3">
                    <h5 className="ml-4">{t("Quick_Payment")}</h5>
                    <Form role="form">
                        <FormGroup>
                            <InputGroup className="mb-3 w-100">

                                {/* <Input value="GOODYEAR TIRE CENTER" type="text" /> */}
                                <UncontrolledDropdown className="w-100">
                                    <DropdownToggle
                                        caret
                                        color="secondary"
                                        id="dropdownMenuButton"
                                        type="button"
                                        className="w-100"
                                        style={{ whiteSpace: "pre-wrap" }}
                                    >
                                        {selectedAmount}
                                    </DropdownToggle>

                                    <DropdownMenu aria-labelledby="dropdownMenuButton">

                                        <DropdownItem href="#pablo" name="amount" onClick={(e) => toggleOption(e)} >
                                            3,782.56(Current Balance)
                                        </DropdownItem>

                                        <DropdownItem href="#pablo" name="amount" onClick={(e) => toggleOption(e)} >
                                            5,782.56(Balance Due)
                                        </DropdownItem>
                                    </DropdownMenu>

                                </UncontrolledDropdown>


                            </InputGroup>
                        </FormGroup>
                        <FormGroup>
                            <InputGroup className="mb-3 w-100">

                                {/* <Input value="GOODYEAR TIRE CENTER" type="text" /> */}
                                <UncontrolledDropdown className="w-100">
                                    <DropdownToggle
                                        caret
                                        color="secondary"
                                        id="dropdownMenuButton"
                                        type="button"
                                        className="w-100"
                                        style={{ whiteSpace: "pre-wrap" }}
                                    >
                                        {selectedCard}
                                    </DropdownToggle>

                                    <DropdownMenu aria-labelledby="dropdownMenuButton">

                                        <DropdownItem href="#pablo" name="card" onClick={(e) => toggleOption(e)} >
                                            VISA(2544)
                                        </DropdownItem>
                                        <DropdownItem href="#pablo" name="card" onClick={(e) => toggleOption(e)} >
                                            MasterCard(4585)
                                        </DropdownItem>
                                    </DropdownMenu>

                                </UncontrolledDropdown>


                            </InputGroup>
                        </FormGroup>
                        <FormGroup>
                            <InputGroup className="mb-3">

                                {/* <Input placeholder="Payment Date" defaultValue="28-DEC-2021" type="date" /> */}
                                <DatePicker
                                    name="payment_date"
                                    className="btn w-100 btn-secondary"
                                    id="example-datepicker"
                                    value={paymentDate}
                                    dateFormat="MM/DD/YYYY"
                                    onChange={(e) => toggleOptionDate(e)} />

                            </InputGroup>
                        </FormGroup>
                        <div className="text-center">
                            <Button className="make-payment my-4" color="primary" type="button">
                                {t("make_A_Payment")}
                            </Button>
                        </div>
                    </Form>

                    <a href="#"><h6 className="text-right  mt-3" style={{ textDecoration: "underline" }}> {t("setup_Auto_Payment")}</h6></a>

                </Col>
            </Row>
        </Card>
    )
}
export default AccountSummary;