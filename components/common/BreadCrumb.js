import React, { useEffect, useState } from "react";
import Link from "next/link";
import {
    Container,
    Row,
    Col,
    Form,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown
} from "reactstrap";
import i18n from "./../Translations/i18n"
import { useTranslation } from "react-i18next";
import Router, { useRouter } from 'next/router';
const BreadCrumb = () => {
    let [selectedStore, setselectedStore] = useState(null);
    const router = useRouter();
    const languageList = process.env.NEXT_PUBLIC_LANGUAGE;

    const toggleOption = (e) => {
        e.preventDefault();
        if (e.target.name == "store") {
            setselectedStore(e.target.innerText);
        }
    };
    const { t } = useTranslation();
    return (
        <Row className="mb-5 m-3">
            <Col xl="6">
                <h6 className="home mb-0">{t('home')}\</h6>
                <h3 className="mb-0">{t("dashboard")}</h3>
                <h4 className="mb-0">{selectedStore ? selectedStore : t("select_store")}</h4>
            </Col>
            <Col xl="6">
                <Form role="form">
                    <div className="mb-3 w-100">
                        <UncontrolledDropdown className="w-100">
                            <DropdownToggle
                                caret
                                color="secondary"
                                id="dropdownMenuButton"
                                type="button"
                                className="w-100"
                                style={{ whiteSpace: "pre-wrap" }}
                            >
                                {selectedStore ? selectedStore : t("select_store")}
                            </DropdownToggle>

                            <DropdownMenu aria-labelledby="dropdownMenuButton">
                                <DropdownItem href="#pablo" name="store" onClick={(e) => toggleOption(e)}>
                                    GOOD YEAR TIRE CENTER OF PUYALLUP-PARTS
                                </DropdownItem>
                                <DropdownItem href="#pablo" name="store" onClick={(e) => toggleOption(e)}>
                                    GOOD YEAR TIRE CENTER OF OHIO-PARTS
                                </DropdownItem>
                                <DropdownItem href="#pablo" name="store" onClick={(e) => toggleOption(e)}>
                                    GOOD YEAR TIRE CENTER OF N. CAROLINA-PARTS
                                </DropdownItem>
                                <DropdownItem href="#pablo" name="store" onClick={(e) => toggleOption(e)}>
                                    GOOD YEAR TIRE CENTER OF INDIANA-PARTS
                                </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </div>
                </Form>
            </Col>
        </Row>
    )
}
export default BreadCrumb;