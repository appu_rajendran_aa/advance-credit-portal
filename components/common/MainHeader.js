import React, { useEffect } from "react";
import Link from "next/link";
import i18n from "./../Translations/i18n"
import { useTranslation } from "react-i18next";
import Router, { useRouter } from 'next/router';
import Image from "next/image";

// reactstrap components
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Navbar,
  Nav,
  Container,
  Media,
} from "reactstrap";
const MainHeader = (props) => {
  const router = useRouter();

  const { t } = useTranslation();
  return (
    <>
      <Navbar style={{ background: "#cc0033" }} className="navbar-top navbar-dark" expand="md" id="navbar-main">
        <Container fluid>

          <div className={'d-flex align-items-center'} style={{ width: "500px" }}>
            <Image alt={props.logo.imgAlt} width='158' height='48' className="navbar-brand-img" src={props.logo.imgSrc} />

            <Link href="/admin/dashboard">
              <a className="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block px-2">
                {props.brandText}
              </a>
            </Link>
          </div>


          <Nav className="align-items-center d-none d-md-flex" navbar>
            <UncontrolledDropdown nav>

              <Media className="align-items-center">

                <Media className="mr-2 ml-2 d-none d-lg-block">
                  <span className="mb-0 text-sm text-white font-weight-bold">
                    {/* <i className="ni ni-support-16 text-white px-2"></i> */}

                    {t("help")}
                  </span>
                </Media>

                <Media className="mr-2 ml-2 d-none d-lg-block align-items-center">

                  <span className="mb-0 text-sm text-white font-weight-bold">
                    {/* <i className="ni ni-settings-gear-65 text-white px-2"></i> */}
                    {t("preferences")}
                  </span>
                </Media>

                <Media className="mr-2 ml-2 d-none d-lg-block">


                  <span className="mb-0 text-sm text-white font-weight-bold">
                    {/* <i className=" ni ni-bold-right text-white px-2"></i> */}
                    {t("signout")}
                  </span>
                </Media>
              </Media>

            </UncontrolledDropdown>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
}

export default MainHeader;