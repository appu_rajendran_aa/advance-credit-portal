// Import Highcharts
import Highcharts from "highcharts";
import HighchartsReact from 'highcharts-react-official'
import drilldown from "highcharts/modules/drilldown.js";
import React, { useState } from "react";
if (typeof Highcharts === 'object') {
    drilldown(Highcharts);
}


const pie = ({ availablePercentage }) => {


    var chartOptions = {
        chart: {
            type: 'pie',
        },
        title: {
            text: `${availablePercentage} %`,
            align: 'center',
            verticalAlign: 'middle',
            y: 18,// change this to 0
            style: {
                fontSize: '15px'
            }
        },
        credits: {
            enabled: false
        },
        series: [
            {
                name: "Open To Buy",
                colorByPoint: true,
                size: '60%',
                innerSize: '30%',

                data: [
                    {
                        name: "Available",
                        y: availablePercentage,
                        color: "#cc0033"
                    },
                    {
                        name: "Closed",
                        y: 100 - availablePercentage,
                    },
                ]
            }
        ],
    }

    return <HighchartsReact
        highcharts={Highcharts}
        options={chartOptions}
    />
        ;


}
export default pie