import React, { useEffect } from "react";
import { Grid, h } from 'gridjs-react';
import "gridjs/dist/theme/mermaid.css";
// reactstrap components
import {
    Row,
    Col,
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    Container
} from "reactstrap";
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
const invoiceModal = (props) => {
    return (
        <Modal toggle={() => props.toggle()} isOpen={props.modalOpen} size="lg" style={{ maxWidth: '90%', marginTop: "120px", width: '100%' }} >
            <div className=" modal-header" style={{ width: "'90%'" }}>
                <h3 className=" modal-title" id="exampleModalLabel">
                    Invoice Details
                </h3>
                <button
                    aria-label="Close"
                    className=" close"
                    type="button"
                    onClick={() => props.toggle()}
                >
                    <span aria-hidden={true}>×</span>
                </button>
            </div>{console.log(props.modalData)}
            <ModalBody >
                {props.modalData ?
                    <Container fluid>
                        <h2>Transaction Header</h2>
                        <Row>
                            <Col xl="3">
                                <div className="d-flex justify-content-between">
                                    <h5> Coustomer Account Number</h5>
                                    <h5>{props.modalData.customerAccountNumber}</h5>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5> Transaction Amount</h5>
                                    <h5>$ {props.modalData.originalAmount}</h5>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5>Payment Term</h5>
                                    <h5>{props.modalData.paymentTerms}</h5>
                                </div>
                            </Col>
                            <Col xl="3">
                                <div className="d-flex justify-content-between">
                                    <h5> Transaction #</h5>
                                    <h5>{props.modalData.transactionId}</h5>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5> Due Date</h5>
                                    <h5>{props.modalData.dueDate}</h5>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5>Transaction Status</h5>
                                    <h5>{props.modalData.transactionStatus}</h5>
                                </div>
                            </Col>
                            <Col xl="3">
                                <div className="d-flex justify-content-between">
                                    <h5> Type</h5>
                                    <h5>{props.modalData.transactionType}</h5>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5> Transaction Balance</h5>
                                    <h5>$ {props.modalData.balanceAmount}</h5>
                                </div>
                            </Col>
                            <Col xl="3">
                                <div className="d-flex justify-content-between">
                                    <h5> Transaction date</h5>
                                    <h5>{props.modalData.transactionDate}</h5>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5> PO #</h5>
                                    <h5>{props.modalData.purchaseOrder}</h5>
                                </div>
                            </Col>
                        </Row>
                        <h2>Transaction Details</h2>
                        <Row className="justify-content-center">
                            <Col className="mb-5 mb-xl-0" xl="12">
                                <Grid
                                    data={[
                                        ['', '', '', '', '', '', '', ''],
                                    ]}
                                    columns={['Line Number', 'Product Line', 'Description', 'SKU', 'Quantity', 'UOM', 'Unit Price', 'Line Amount']}
                                    search={false}
                                    sort={false}
                                    resizable={true}
                                    pagination={{
                                        enabled: false,
                                        limit: 7,
                                    }}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xl="8"></Col>
                            <Col xl="4" className=" justify-content-end">
                                <div className="d-flex justify-content-between">
                                    <h5> Sub Total</h5>
                                    <h5> $ </h5>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5>Total Sales / Use Tax</h5>
                                    <h5>$ </h5>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5>Total Transaction Amount</h5>
                                    <h5>$ </h5>
                                </div>
                            </Col>
                        </Row>
                        <h2>Payment Details</h2>
                        <Row className="justify-content-center">
                            <Col className="mb-5 mb-xl-0" xl="12">
                                <Grid
                                    data={[
                                        ['', '', '', '', '', ''],
                                    ]}
                                    columns={['Payment Date', 'Applicaction Type', 'Transaction Number', 'Applied Date', 'Quantity', 'Amount Applied']}
                                    search={false}
                                    sort={false}
                                    resizable={true}
                                    pagination={{
                                        enabled: false,
                                        limit: 7,
                                    }}
                                />
                            </Col>
                        </Row>
                    </Container>
                    :
                    <Skeleton height={30} />
                }
            </ModalBody>
            <ModalFooter>
                <Button
                    color="secondary"
                    type="button"
                    onClick={() => props.toggle()}
                >
                    Cancel
                </Button>
            </ModalFooter>
        </Modal>
    )
}
export default invoiceModal;