import { Grid } from 'gridjs-react';
import "gridjs/dist/theme/mermaid.css";
import Link from "next/link";
const TransationGrid = ({ data, switchModal }) => {


  return (
    <div className="justify-content-center" style={{ height: 300, width: "100%" }}>


      <Grid id='data-grid'
        data={data}
        columns={[{
          data: (data) => data.transactionDate,
          name: "Date",
        },
        {
          data: (data) => data.transactionType,
          name: "Type",
        },
        {
          data: (data) => data.purchaseOrder,
          name: "PO #",
          attributes: (cell) => {
            // add these attributes to the td elements only
            if (cell) {
              return {
                'data-cell-content': cell,
                'onclick': () => {
                  switchModal(cell)
                }
              };
            }
          }
        },
        {
          data: (data) => data.transactionNumber,
          name: "Invoice #",
        },
        {
          data: (data) => data.originalAmount,
          name: "Amount",
        },
        {
          data: (data) => data.transactionStatus,
          name: "Status",
        },
        ]}
        search={false}

        sort={true}
        resizable={true}
        pagination={{
          enabled: false,
          limit: 3,
        }}
      />
    </div>
  )
}
export default TransationGrid
